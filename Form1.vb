﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports System.IO
Imports System.IO.Compression
Imports Microsoft.VisualBasic.FileIO

Public Class frmPrincipale
    Public setupIni As New INIFile(Application.StartupPath & "\setup.ini")
    Public dbFolder As String = setupIni.ReadValue("Option", "databasefolder")
    Public fwFolder As String = setupIni.ReadValue("Option", "firmwarefolder")
    Public databaseName As String = setupIni.ReadValue("Option", "databasename")
    Public path7Zip As String = setupIni.ReadValue("Option", "7ZipPath")
    Public lastModule As String = UCase(setupIni.ReadValue("Option", "lastModule"))
    Public stringaConnessione As String = "provider=Microsoft.Jet.OLEDB.4.0;Data Source =" & dbFolder & databaseName
    Public objConn As OleDbConnection = New OleDbConnection(stringaConnessione)
    Public sqlStringa As String = ""
    Public sqlCommand As OleDbCommand
    Public dbReader As OleDbDataReader
    Public suiteCode As String
    Public fileRead As StreamReader
    Public toolTip As ToolTip

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim swUpdater As String = setupIni.ReadValue("Option", "SWToolUpdater") 'Nome file eseguibile dell'aggiornatore
        If File.Exists(Application.StartupPath & "\" & swUpdater) = True Then
            Dim currentVersion, updateVersion As Double
            Dim updateFolder As String = setupIni.ReadValue("Option", "SWToolUpdateFolder") 'Cartella contenente i file aggiornati
            If Mid(updateFolder, Len(updateFolder) - 1, 1) <> "\" Then updateFolder &= "\"
            Dim permissionIni As New INIFile(updateFolder & "..\SWTUpdate.ini") 'File ini con autorizzazione PC agli aggiornamenti
            Dim pcAutorizzato As String = UCase(permissionIni.ReadValue("FWIdentificator", "Tutti")) 'Veifica autorizzazione
            If pcAutorizzato <> "SI" Then
                pcAutorizzato = UCase(permissionIni.ReadValue("FWIdentificator", System.Environment.MachineName)) 'Veifica autorizzazione
            End If
            If pcAutorizzato = "SI" Then
                Dim nomeFile As String = Mid(Application.ExecutablePath, Application.ExecutablePath.LastIndexOf("\") + 2) 'Nome file eseguibile SW da aggiornare

                If File.Exists(updateFolder & nomeFile) = True Then
                    currentVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(Application.StartupPath & "\" & nomeFile).ProductVersion 'Versione file locale
                    updateVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(updateFolder & nomeFile).ProductVersion 'Versione file aggiornato
                    If currentVersion <> updateVersion Then
                        Dim startInfo As New ProcessStartInfo
                        startInfo.FileName = Application.StartupPath & "\" & swUpdater
                        If InStr(updateFolder, " ") > 0 Then
                            updateFolder = """" & updateFolder & """"
                        End If
                        Dim localFolder As String = Application.StartupPath & "\"
                        If InStr(localFolder, " ") > 0 Then
                            localFolder = """" & Application.StartupPath & """"
                        End If
                        If InStr(nomeFile, " ") > 0 Then
                            nomeFile = """" & nomeFile & """"
                        End If
                        startInfo.Arguments = updateFolder & " " & localFolder & "\ " & nomeFile
                        Process.Start(startInfo)
                        End
                    End If
                End If
            End If
        End If

        cboModulo.Items.Clear()
        cboModulo.Items.Add("CASSETTES")
        cboModulo.Items.Add("READER")
        cboModulo.Items.Add("SAFE CONTROLLER")
        cboModulo.Items.Add("CONTROLLER")
        cboModulo.Items.Add("CD80")
        cboModulo.Items.Add("MBAG")
        cboModulo.Items.Add("FPGA")
        cboModulo.Items.Add("BOOT")

        cboModulo.SelectedIndex = 0
        For i = 0 To cboModulo.Items.Count - 1
            If cboModulo.Items(i) = lastModule Then
                cboModulo.SelectedIndex = i
                Exit For
            End If
        Next

        toolTip = New ToolTip
        toolTip.AutoPopDelay = 10000
        toolTip.InitialDelay = 500
        toolTip.ReshowDelay = 300
        toolTip.ShowAlways = True
        SvuotaCartella(Application.StartupPath & "\download")
        Me.Text &= " V" & Application.ProductVersion
        objConn.Open()
        sqlStringa = "SELECT DISTINCT tblman.id_prodotto FROM tblman, prodotti WHERE tblman.id_prodotto = prodotti.id_prodotto And (prodotti.categoria_prodotto Like 'SPAREBOARD' OR prodotti.categoria_prodotto Like 'CM18%' OR prodotti.categoria_prodotto LIKE 'CM20%' OR prodotti.categoria_prodotto LIKE 'OM61%')"

        Dim sqlCommand As New OleDbCommand(sqlStringa, objConn)
        dbReader = sqlCommand.ExecuteReader
        cboCode.Items.Clear()
        Do While Not dbReader.Read() = Nothing
            cboCode.Items.Add(dbReader("id_prodotto"))
        Loop
        objConn.Close()
    End Sub

    Sub SelezionaProdotto()
        lstClient.Items.Clear()
        lblProduct.Text = "-"
        cmdSearch.Enabled = False
        Try
            objConn.Open()
            sqlStringa = "SELECT DISTINCT clienti.nome_cliente FROM tblman, clienti WHERE id_prodotto = '" & cboCode.Text & "' AND clienti.id_cliente = tblman.id_cliente "
            sqlCommand = New OleDbCommand(sqlStringa, objConn)
            dbReader = sqlCommand.ExecuteReader
            Do While Not dbReader.Read() = Nothing
                lstClient.Items.Add(dbReader("nome_cliente"))
            Loop
            sqlStringa = "SELECT * FROM prodotti WHERE id_prodotto = '" & cboCode.Text & "'"
            sqlCommand = New OleDbCommand(sqlStringa, objConn)
            dbReader = sqlCommand.ExecuteReader
            Do While Not dbReader.Read() = Nothing
                lblProduct.Text = dbReader("categoria_prodotto")
            Loop
            objConn.Close()
            lstClient.SelectedIndex = 0
        Catch ex As Exception
            lblProduct.Text = ex.Message
        End Try
    End Sub

    Private Sub cboCode_LostFocus(sender As Object, e As EventArgs) Handles cboCode.LostFocus
        'Try
        '    For i = 0 To cboCode.Items.Count - 1
        '        If cboCode.Text = cboCode.Items(i) Then
        '            cboCode.SelectedIndex = i
        '            Exit For
        '        End If
        '    Next
        'Catch ex As Exception
        '    lblProduct.Text = ex.Message
        'End Try
        'SelezionaProdotto()
    End Sub
    Private Sub cboCode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCode.SelectedIndexChanged
        SelezionaProdotto()
    End Sub
    Private Sub cboCode_TextChanged(sender As Object, e As EventArgs) Handles cboCode.TextChanged
        'lstClient.Items.Clear()
    End Sub

    Private Sub lstClient_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstClient.SelectedIndexChanged
        cmdSearch.Enabled = True

        'identifica il fw da copiare
        objConn.Open()

        Dim idCliente As Integer
        sqlStringa = "SELECT * FROM clienti WHERE nome_cliente = '" & lstClient.SelectedItem & "'"
        sqlCommand = New OleDbCommand(sqlStringa, objConn)
        dbReader = sqlCommand.ExecuteReader
        Do While Not dbReader.Read() = Nothing
            idCliente = (dbReader("id_cliente"))
        Loop

        sqlStringa = "SELECT * FROM tblman WHERE id_prodotto = '" & cboCode.Text & "' AND id_cliente = " & idCliente & " AND id_modulo = 'FW_PRODOTTO'"
        sqlCommand = New OleDbCommand(sqlStringa, objConn)
        dbReader = sqlCommand.ExecuteReader
        Do While Not dbReader.Read() = Nothing
            suiteCode = dbReader("cod_fw")
            lblSuite.Text = suiteCode
        Loop
        objConn.Close()

    End Sub

    ''' <summary>
    ''' Analizza il file alla ricerca dei FW
    ''' </summary>
    ''' <param name="nomeFile">File da aprire</param>
    ''' <param name="modulo">Modulo da cercare</param>
    ''' <returns>sdfsdsdf sdf</returns>
    Private Function AnalizzaFile(ByVal nomeFile As String, ByVal modulo As String) As String
        fileRead = New StreamReader(nomeFile)
        Dim cercaFw As String = ""
        Dim riga As String = ""
        AnalizzaFile = ""
        Select Case modulo
            Case "CASSETTES"
                cercaFw = "AB"
            Case "READER"
                cercaFw = "3="
            Case "SAFE CONTROLLER"
                cercaFw = "4="
            Case "CONTROLLER"
                cercaFw = "2="
            Case "CD80"
                cercaFw = "A="
            Case "MBAG"
                cercaFw = "a="
            Case "FPGA"
                cercaFw = "5="
            Case "BOOT"
                cercaFw = "6="
        End Select

        Do While fileRead.Peek() >= 0
            riga = fileRead.ReadLine()
            If Mid(riga, 1, 2) = cercaFw Then
                Dim i As Integer = 1
                Do Until Mid(riga, i, 1) = "="

                    i += 1
                Loop
                If AnalizzaFile.Length > 0 Then AnalizzaFile &= Chr(13)
                AnalizzaFile &= Mid(riga, i + 1, Len(riga) - i)
                'Exit Do
            End If
        Loop
        fileRead.Close()
        If AnalizzaFile.Length = 0 Then
            AnalizzaFile = "FILE NOT FOUND"
        End If
    End Function

    Private Sub cboModulo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboModulo.SelectedIndexChanged
        lblModuleName.Text = cboModulo.Text
        lblModuleFW.Text = ""
        If lstClient.Enabled = False Then
            CercaFile()
        End If
        setupIni.WriteValue("Option", "lastModule", cboModulo.Text)
    End Sub


    Private Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        cboCode.Enabled = False
        lstClient.Enabled = False

        SvuotaCartella(Application.StartupPath & "\download")

        For Each file As String In My.Computer.FileSystem.GetFiles(fwFolder & suiteCode, FileIO.SearchOption.SearchTopLevelOnly, "*.zip")
            Dim p As New Process
            p.StartInfo.UseShellExecute = True
            p.StartInfo.FileName = path7Zip & "\7z.exe"
            p.StartInfo.Arguments = " x " & """" & file & """" & " -o" & """" & Application.StartupPath & "\Download" & """" & " -aoa"
            p.StartInfo.RedirectStandardOutput = False
            p.Start()
            p.WaitForExit()
        Next
        CercaFile()
        cmdSearch.Enabled = False
        cmdReset.Enabled = True
        Me.Focus()
    End Sub
    Private Sub CercaFile()
        Dim di As New DirectoryInfo(Application.StartupPath & "\Download")
        Dim fi As FileInfo

        For Each fi In di.GetFiles
            If System.IO.Path.GetExtension(fi.FullName).ToLower = ".upg" Then
                lblModuleFW.Text = AnalizzaFile(fi.FullName, cboModulo.Text)
                lblModuleFW.Text = Mid(fi.Name, 1, Len(fi.Name) - 4) & Chr(13) & lblModuleFW.Text
                Exit Sub
            End If
        Next
    End Sub

    Private Sub SvuotaCartella(ByVal cartella As String)
        Dim folder As DirectoryInfo = New DirectoryInfo(cartella)
        Dim nTentativi As Integer = 0
        Try
CancellaCartella:
            For Each File As IO.FileInfo In folder.GetFiles
                File.Delete()
            Next
            For Each Dir As IO.DirectoryInfo In folder.GetDirectories
                My.Computer.FileSystem.DeleteDirectory(Dir.FullName(), FileIO.DeleteDirectoryOption.DeleteAllContents)
            Next

            If Directory.GetFiles(Application.StartupPath & "\download").Count > 0 Then
                nTentativi += 1
                If nTentativi > 1 Then
                    MsgBox("Non è stato possibile svuotare la cartella " & Application.StartupPath & "\download" & ", il programma verrà terminato")
                    End
                End If
                GoTo CancellaCartella
            End If
        Catch ex As DirectoryNotFoundException
            Directory.CreateDirectory(Application.StartupPath & "\Download")
        End Try
    End Sub

    Private Sub cmdReset_Click(sender As Object, e As EventArgs) Handles cmdReset.Click
        cmdSearch.Enabled = False
        'cboModulo.SelectedIndex = 0
        cboCode.SelectedIndex = -1
        cboCode.Enabled = True
        lstClient.Items.Clear()
        lstClient.Enabled = True
        lblModuleFW.Text = ""
        lblProduct.Text = ""

        '        SvuotaCartella(Application.StartupPath & "\download")
        cboCode.Focus()
    End Sub

    Private Sub lblModuleFW_ClientSizeChanged(sender As Object, e As EventArgs) Handles lblModuleFW.ClientSizeChanged
        If lblModuleFW.Text.Length > 0 Then
            toolTip.SetToolTip(lblModuleFW, lblModuleFW.Text)
        End If
    End Sub

    Private Sub frmPrincipale_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        'SvuotaCartella(Application.StartupPath & "\download")
    End Sub

    Private Sub cboCode_KeyUp(sender As Object, e As KeyEventArgs) Handles cboCode.KeyUp
        If e.KeyCode = 13 Then
            lstClient.Focus()
        End If
    End Sub

    Private Sub frmPrincipale_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        SvuotaCartella(Application.StartupPath & "\download")
    End Sub
End Class
