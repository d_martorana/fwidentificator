!define APPNAME "FW Identificator"
!define COMPANYNAME "ARCA Techologies"
!define VERSIONMAJOR 1
!define VERSIONMINOR 0
!define VERSIONBUILD 1

OutFile "${APPNAME} V${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}.exe"
;RequestExecutionLevel admin



Section "install"
	;Banner::show /set 76 "Banner Visualizzato" /set 54 "Normal Text"
	SetOutPath "C:\ARCA\FW Identificator"
	File "C:\Users\Utente\Documents\Visual Studio 2015\Projects\FW Identificator\bin\x86\Release\FW Identificator.exe"
	File "C:\Users\Utente\Documents\Visual Studio 2015\Projects\FW Identificator\bin\x86\Release\Setup.ini"
	WriteUninstaller "C:\ARCA\FW Identificator\Uninstall.exe"
SectionEnd

Section "Uninstall"
	RMDir /r "C:\ARCA\FW Identificator\*.*"
	RMDir "C:\ARCA\FW Identificator"
SectionEnd

Function .onInstSuccess
	MessageBox MB_OK "Installazione eseguita correttamente!"
FunctionEnd

Function un.onUninstSuccess
	MessageBox MB_OK "Disinstallazione eseguita correttamente!"
FunctionEnd